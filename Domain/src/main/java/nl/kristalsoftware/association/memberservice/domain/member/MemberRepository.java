package nl.kristalsoftware.association.memberservice.domain.member;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.MemberAggregateFactory;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateRepository;

@Slf4j
public class MemberRepository extends AggregateRepository<MemberRepository, Member, MemberReference> {

    private final MemberRepositoryService memberRepositoryService;

    private MemberRepository(MemberAggregateFactory memberAggregateFactory,
                             MemberRepositoryService memberRepositoryService) {
        super(memberAggregateFactory, memberRepositoryService);
        this.memberRepositoryService = memberRepositoryService;
    }

    private MemberRepository(MemberReference memberReference,
                           MemberAggregateFactory memberAggregateFactory,
                           MemberRepositoryService memberRepositoryService) {
        super(memberReference, memberAggregateFactory, memberRepositoryService);
        this.memberRepositoryService = memberRepositoryService;
    }

    public static MemberRepository of(MemberAggregateFactory memberAggregateFactory,
                                    MemberRepositoryService memberRepositoryService) {
        return new MemberRepository(memberAggregateFactory, memberRepositoryService);
    }

    public static MemberRepository of(MemberReference memberReference,
                                    MemberAggregateFactory memberAggregateFactory,
                                    MemberRepositoryService memberRepositoryService) {
        return new MemberRepository(memberReference, memberAggregateFactory, memberRepositoryService);
    }

    @Override
    protected MemberRepository getDomainRepository() {
        return this;
    }

    public void save(MemberSignedUp memberSignedUp) {
        memberRepositoryService.saveEvent(memberSignedUp);
    }


    public void save(MemberKindChanged memberKindChanged) {
        memberRepositoryService.saveEvent(getAggregate(), memberKindChanged);
    }

    public void save(AddressAssignedToMember addressAssignedToMember) {
        memberRepositoryService.saveEvent(getAggregate(), addressAssignedToMember);
    }

    public void save(AddressUnAssignedFromMember addressUnAssignedFromMember) {
        memberRepositoryService.saveEvent(getAggregate(), addressUnAssignedFromMember);
    }
}
