package nl.kristalsoftware.association.memberservice.domain.member;

import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberEventsRepositoryFactoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberEventsRepositoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.association.memberservice.domain.member.streams.MemberEventsStreamingFactoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.streams.MemberEventsStreamingPort;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberDocumentsRepositoryFactoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberDocumentsRepositoryPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateRepositoryService;
import nl.kristalsoftware.ddd.domain.base.event.EventsRepositoryPort;
import nl.kristalsoftware.ddd.domain.base.view.DocumentsRepositoryPort;
import org.springframework.stereotype.Service;

@Service
public class MemberRepositoryService implements AggregateRepositoryService<Member> {
    private final MemberEventsRepositoryPort memberEventsRepositoryPort;
    private final MemberDocumentsRepositoryPort memberDocumentsRepositoryPort;
    private final MemberEventsStreamingPort memberEventsStreamingPort;

    public MemberRepositoryService(MemberEventsRepositoryFactoryPort memberEventsRepositoryFactoryPort,
                                   MemberDocumentsRepositoryFactoryPort memberDocumentsRepositoryFactoryPort,
                                   MemberEventsStreamingFactoryPort memberEventsStreamingFactoryPort) {
        memberEventsRepositoryPort = memberEventsRepositoryFactoryPort.createRepositoryAdapter();
        memberDocumentsRepositoryPort = memberDocumentsRepositoryFactoryPort.createRepositoryAdapter();
        memberEventsStreamingPort = memberEventsStreamingFactoryPort.createStreamingAdapter();
    }

    @Override
    public EventsRepositoryPort<Member> getEventsRepository() {
        return memberEventsRepositoryPort;
    }

    @Override
    public DocumentsRepositoryPort<Member> getDocumentsRepository() {
        return memberDocumentsRepositoryPort;
    }


    public void saveEvent(MemberSignedUp memberSignedUp) {
        memberEventsRepositoryPort.saveEvent(memberSignedUp);
        memberDocumentsRepositoryPort.saveDocument(memberSignedUp);
        memberEventsStreamingPort.produceEvent(memberSignedUp);
    }

    public void saveEvent(Member member, AddressAssignedToMember addressAssignedToMember) {
        memberEventsRepositoryPort.saveEvent(addressAssignedToMember);
        memberDocumentsRepositoryPort.saveDocument(addressAssignedToMember);
        memberEventsStreamingPort.produceEvent(member, addressAssignedToMember);
    }

    public void saveEvent(Member member, AddressUnAssignedFromMember addressUnAssignedFromMember) {
        memberEventsRepositoryPort.saveEvent(addressUnAssignedFromMember);
        memberDocumentsRepositoryPort.saveDocument(addressUnAssignedFromMember);
        memberEventsStreamingPort.produceEvent(member, addressUnAssignedFromMember);
    }

    public void saveEvent(Member member, MemberKindChanged memberKindChanged) {
        memberEventsRepositoryPort.saveEvent(memberKindChanged);
        memberDocumentsRepositoryPort.saveDocument(memberKindChanged);
        memberEventsStreamingPort.produceEvent(member, memberKindChanged);
    }
}
