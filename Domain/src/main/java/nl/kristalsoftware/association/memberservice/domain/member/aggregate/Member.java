package nl.kristalsoftware.association.memberservice.domain.member.aggregate;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.address.AddressDomainEntity;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.commands.AssignAddressToMember;
import nl.kristalsoftware.association.memberservice.domain.member.commands.ChangeMemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.commands.SignUpMember;
import nl.kristalsoftware.association.memberservice.domain.member.commands.UnAssignAddressFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.commands.UpdateMemberAttributes;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.ddd.domain.base.aggregate.BaseAggregateRoot;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

@Slf4j
public class Member extends BaseAggregateRoot<MemberReference> {

    private MemberDomainEntity memberDomainEntity = new MemberDomainEntity();

    private Member(MemberReference memberReference, Boolean existingAggregate) {
        super(memberReference, existingAggregate);
    }

    private Member(MemberReference memberReference, Long version) {
        super(memberReference, version);
    }

    public static Member of(MemberReference memberReference, Boolean existingAggregate) {
        return new Member(memberReference, existingAggregate);
    }

    public static Member of(MemberReference memberReference, Long version) {
        return new Member(memberReference, version);
    }

    public List<DomainEventSaving<MemberRepository>> handleCommand(SignUpMember signUpMember) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        domainEventList.add(MemberSignedUp.of(
                getReference(),
                signUpMember.getMemberName(),
                signUpMember.getMemberBirthDate(),
                signUpMember.getMemberKind()
                )
        );
        return domainEventList;
    }

    public List<DomainEventSaving<MemberRepository>> handleCommand(ChangeMemberKind changeMemberKind) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        domainEventList.add(MemberKindChanged.of(getReference(), changeMemberKind.getMemberKind()));
        return domainEventList;
    }

    public List<DomainEventSaving<MemberRepository>> handleCommand(AssignAddressToMember assignAddressToMember) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        domainEventList.add(AddressAssignedToMember.of(
                getReference(),
                AddressReference.of(UUID.randomUUID()),
                assignAddressToMember.getAddress().getStreet(),
                assignAddressToMember.getAddress().getStreetNumber(),
                assignAddressToMember.getAddress().getZipCode(),
                assignAddressToMember.getAddress().getCity()));
        return domainEventList;
    }

    public List<DomainEventSaving<MemberRepository>> handleCommand(UnAssignAddressFromMember unAssignAddressFromMember) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        domainEventList.add(AddressUnAssignedFromMember.of(
                getReference(),
                unAssignAddressFromMember.getAddressReference())
        );
        return domainEventList;
    }

    public List<DomainEventSaving<MemberRepository>> handleCommand(UpdateMemberAttributes updateMemberAttributes) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        return domainEventList;
    }

    private List<AddressDomainEntity> in(List<AddressDomainEntity> list, Predicate<AddressDomainEntity> predicate) {
        return list.stream()
                .filter(predicate)
                .toList();
    }

    public void loadEventData(MemberSignedUp memberSignedUp) {
        memberDomainEntity.setMemberName(memberSignedUp.getMemberName());
        memberDomainEntity.setMemberBirthDate(memberSignedUp.getMemberBirthDate());
        memberDomainEntity.setMemberKind(memberSignedUp.getMemberKind());
    }

    public void loadEventData(MemberKindChanged memberKindChanged) {
        memberDomainEntity.setMemberKind(memberKindChanged.getMemberKind());
    }

    public void loadEventData(AddressAssignedToMember addressAssignedToMember) {
        memberDomainEntity.addAddress(AddressDomainEntity.of(
                addressAssignedToMember.getAddressReference(),
                addressAssignedToMember.getStreet(),
                addressAssignedToMember.getStreetNumber(),
                addressAssignedToMember.getZipCode(),
                addressAssignedToMember.getCity()
        ));
    }

    public void loadEventData(AddressUnAssignedFromMember addressUnAssignedFromMember) {
        memberDomainEntity.removeAddress(addressUnAssignedFromMember.getAddressReference());
    }

    public MemberName getMemberName() {
        return memberDomainEntity.getMemberName();
    }

    public MemberBirthDate getMemberBirthDate() {
        return memberDomainEntity.getMemberBirthDate();
    }

    public MemberKind getMemberKind() {
        return memberDomainEntity.getMemberKind();
    }

    public List<AddressDomainEntity> getAddresses() {
        return memberDomainEntity.getAddresses();
    }

    public List<AddressDomainEntity> getAddressesNoLongerAssigned(List<Address> memberAddresses) {
        return memberDomainEntity.getAddressesNoLongerAssigned(memberAddresses);
    }

}
