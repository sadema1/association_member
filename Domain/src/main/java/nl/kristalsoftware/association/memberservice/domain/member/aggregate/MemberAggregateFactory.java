package nl.kristalsoftware.association.memberservice.domain.member.aggregate;

import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MemberAggregateFactory implements AggregateFactory<Member,MemberReference> {
    public Member createAggregate() {
        return Member.of(MemberReference.of(UUID.randomUUID()), false);
    }

    public Member createAggregate(MemberReference memberReference) {
        return Member.of(memberReference, true);
    }
}
