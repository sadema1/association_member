package nl.kristalsoftware.association.memberservice.domain.member.aggregate;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.address.AddressDomainEntity;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.Kind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.ddd.domain.base.annotation.AggregateRoot;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEntity;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@AggregateRoot
@DomainEntity
@Data
public class MemberDomainEntity {

    private MemberName memberName;
    private MemberBirthDate memberBirthDate;
    private MemberKind memberKind;
    @Setter(AccessLevel.NONE)
    private List<AddressDomainEntity> addresses;

    public MemberDomainEntity() {
        memberName = MemberName.of("", "");
        memberBirthDate = MemberBirthDate.of(Instant.now());
        memberKind = MemberKind.of(Kind.PLAYER);
        addresses = new ArrayList<>();
    }

    public void addAddress(AddressDomainEntity addressDomainEntity) {
        addresses.add(addressDomainEntity);
    }

    public void removeAddress(AddressReference addressReference) {
        addresses.stream()
                .filter(it -> it.getReference().equals(addressReference))
                .findFirst()
                .ifPresent(it -> addresses.remove(it));
    }

    public List<AddressDomainEntity> getAddressesNoLongerAssigned(List<Address> addresses) {
        var addressNotFound = new AddressNotFoundPredicate(addresses);
        return getAddresses().stream()
                .filter(addressNotFound)
                .toList();
    }

    @RequiredArgsConstructor
    private class AddressNotFoundPredicate implements Predicate<AddressDomainEntity> {

        private final List<Address> addressList;

        @Override
        public boolean test(AddressDomainEntity addressDomainEntity) {
            return !addressList.stream()
                    .anyMatch(it -> it.getAddressReference().equals(addressDomainEntity.getReference()));
        }
    }

}
