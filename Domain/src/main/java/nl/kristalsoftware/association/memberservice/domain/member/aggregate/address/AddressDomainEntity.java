package nl.kristalsoftware.association.memberservice.domain.member.aggregate.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.City;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Street;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.StreetNumber;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.ZipCode;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEntity;

@DomainEntity
@AllArgsConstructor
@Data
public class AddressDomainEntity {
    private AddressReference reference;
    private Street street;
    private StreetNumber streetNumber;
    private ZipCode zipCode;
    private City city;

    public static AddressDomainEntity of(
            AddressReference addressReference,
            Street street,
            StreetNumber streetNumber,
            ZipCode zipCode,
            City city
    ) {
        return new AddressDomainEntity(
                addressReference,
                street,
                streetNumber,
                zipCode,
                city
        );
    }
}
