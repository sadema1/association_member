package nl.kristalsoftware.association.memberservice.domain.member.attributes;

public enum Kind {

    PLAYER, SUPPORTING_MEMBER

}
