package nl.kristalsoftware.association.memberservice.domain.member.attributes.address;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder(toBuilder = true)
@Getter
@RequiredArgsConstructor(staticName = "of")
public class Address {
    private final AddressReference addressReference;
    private final Street street;
    private final StreetNumber streetNumber;
    private final ZipCode zipCode;
    private final City city;
}
