package nl.kristalsoftware.association.memberservice.domain.member.attributes.address;


import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;

import java.util.UUID;

@ValueObject
public class AddressReference extends TinyUUIDType {

    private AddressReference(UUID value) {
        super(value);
    }

    public static AddressReference of(UUID value) {
        return new AddressReference(value);
    }

    public static AddressReference of(String value) {
        UUID uuid = null;
        if (value != null && !value.isEmpty() && !value.equals("0")) {
            uuid = UUID.fromString(value);
        }
        return new AddressReference(uuid);
    }

}
