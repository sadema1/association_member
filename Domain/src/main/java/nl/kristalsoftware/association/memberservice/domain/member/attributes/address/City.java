package nl.kristalsoftware.association.memberservice.domain.member.attributes.address;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class City extends TinyStringType {

    private City(String value) {
        super(value);
    }

    public static City of(String city) {
        return new City(city);
    }

}
