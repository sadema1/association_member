package nl.kristalsoftware.association.memberservice.domain.member.attributes.address;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class Street extends TinyStringType {

    private Street(String value) {
        super(value);
    }

    public static Street of(String street) {
        return new Street(street);
    }

}
