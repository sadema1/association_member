package nl.kristalsoftware.association.memberservice.domain.member.attributes.address;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class StreetNumber extends TinyStringType {

    private StreetNumber(String value) {
        super(value);
    }

    public static StreetNumber of(String streetNumber) {
        return new StreetNumber(streetNumber);
    }

}
