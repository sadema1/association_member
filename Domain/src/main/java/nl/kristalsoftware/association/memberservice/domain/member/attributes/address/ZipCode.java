package nl.kristalsoftware.association.memberservice.domain.member.attributes.address;

import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyStringType;

@ValueObject
public class ZipCode extends TinyStringType {

    private ZipCode(String value) {
        super(value);
    }

    public static ZipCode of(String zipCode) {
        return new ZipCode(zipCode);
    }

}
