package nl.kristalsoftware.association.memberservice.domain.member.commands;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommand;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.List;

@RequiredArgsConstructor(staticName = "of")
public class AssignAddressToMember implements BaseCommand<MemberRepository, Member> {

    @Getter
    private final Address address;

    @Override
    public List<DomainEventSaving<MemberRepository>> handleCommand(Member member) {
        return member.handleCommand(this);
    }
}
