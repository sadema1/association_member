package nl.kristalsoftware.association.memberservice.domain.member.commands;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepositoryService;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.MemberAggregateFactory;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.address.AddressDomainEntity;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainService;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommandService;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
@DomainService
public class MemberCommandService extends BaseCommandService<MemberRepository, Member> {

    private final MemberAggregateFactory memberAggregateFactory;
    private final MemberRepositoryService memberRepositoryService;

    public Optional<MemberReference> signUpMember(
            MemberName memberName,
            MemberBirthDate memberBirthDate,
            MemberKind memberKind) {
        MemberRepository memberRepository = MemberRepository.of(memberAggregateFactory, memberRepositoryService);
        List<DomainEventSaving<MemberRepository>> domainEventList = sendCommand(SignUpMember.of(memberName, memberBirthDate, memberKind), memberRepository.getAggregate());
        if (memberRepository.saveEvents(domainEventList)) {
            return Optional.of(memberRepository.getAggregate().getReference());
        }
        return Optional.empty();
    }

    public void editMember(
            MemberReference memberReference,
            MemberName memberName,
            MemberBirthDate memberBirthDate,
            MemberKind memberKind,
            List<Address> memberAddresses) throws AggregateNotFoundException {
        MemberRepository memberRepository = MemberRepository.of(memberReference, memberAggregateFactory, memberRepositoryService);
        List<DomainEventSaving<MemberRepository>> domainEventList = evaluateMemberKindChanged(memberRepository.getAggregate(), memberKind);
        domainEventList.addAll(evaluateAddressesNoLongerAssigned(memberRepository.getAggregate(), memberAddresses));
        domainEventList.addAll(evaluateNewAssignedAddresses(memberRepository.getAggregate(), memberAddresses));
        domainEventList.addAll(evaluateAddressAttributes(memberRepository.getAggregate(), memberAddresses));
        domainEventList.addAll(evaluateAttributes(memberRepository.getAggregate(), memberName, memberBirthDate));
        memberRepository.saveEvents(domainEventList);
    }

    private List<DomainEventSaving<MemberRepository>> evaluateAddressAttributes(Member member, List<Address> memberAddresses) {
        return List.of();
    }

    private List<DomainEventSaving<MemberRepository>> evaluateAttributes(Member member, MemberName memberName, MemberBirthDate memberBirthDate) {
        List<DomainEventSaving<MemberRepository>> domainEventList = sendCommand(UpdateMemberAttributes.of(memberName, memberBirthDate), member);
        return domainEventList;
    }

    private List<DomainEventSaving<MemberRepository>> evaluateNewAssignedAddresses(Member member, List<Address> memberAddresses) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        memberAddresses.forEach(it -> {
            if (it.getAddressReference().isEmpty()) {
                domainEventList.addAll(sendCommand(AssignAddressToMember.of(
                        Address.of(
                                AddressReference.of(UUID.randomUUID()),
                                it.getStreet(),
                                it.getStreetNumber(),
                                it.getZipCode(),
                                it.getCity()
                        )
                ), member));
            }
        });
        return domainEventList;
    }

    private List<DomainEventSaving<MemberRepository>> evaluateAddressesNoLongerAssigned(Member
                                                           member, List<Address> memberAddresses) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        List<AddressDomainEntity> addressesNoLongerAssigned = member.getAddressesNoLongerAssigned(memberAddresses);
        addressesNoLongerAssigned
                .forEach(it -> {
                    domainEventList.addAll(sendCommand(UnAssignAddressFromMember.of(it.getReference()), member));
                });
        return domainEventList;
    }

    private List<DomainEventSaving<MemberRepository>> evaluateMemberKindChanged(Member member, MemberKind memberKind) {
        if (hasMemberKindChanged(member, memberKind)) {
            return sendCommand(ChangeMemberKind.of(memberKind), member);
        }
        return List.of();
    }

    private boolean hasMemberKindChanged(Member member, MemberKind memberKind) {
        return !member.getMemberKind().equals(memberKind);
    }

}
