package nl.kristalsoftware.association.memberservice.domain.member.commands;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommand;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
public class UpdateMemberAttributes implements BaseCommand<MemberRepository, Member> {

    private MemberName memberName;

    private MemberBirthDate memberBirthDate;

    @Override
    public List<DomainEventSaving<MemberRepository>> handleCommand(Member member) {
        return member.handleCommand(this);
    }
}
