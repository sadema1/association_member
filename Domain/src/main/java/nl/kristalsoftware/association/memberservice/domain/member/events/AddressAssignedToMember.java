package nl.kristalsoftware.association.memberservice.domain.member.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.City;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Street;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.StreetNumber;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.ZipCode;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class AddressAssignedToMember implements DomainEventLoading<Member>, DomainEventSaving<MemberRepository> {

    private final MemberReference memberReference;
    private final AddressReference addressReference;
    private final Street street;
    private final StreetNumber streetNumber;
    private final ZipCode zipCode;
    private final City city;

    @Override
    public void load(Member aggregate) {
        aggregate.loadEventData(this);
    }

    @Override
    public void save(MemberRepository repository) {
        repository.save(this);
    }
}
