package nl.kristalsoftware.association.memberservice.domain.member.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class AddressUnAssignedFromMember implements DomainEventLoading<Member>, DomainEventSaving<MemberRepository> {

    private final MemberReference memberReference;
    private final AddressReference addressReference;

    @Override
    public void load(Member aggregate) {
        aggregate.loadEventData(this);
    }

    @Override
    public void save(MemberRepository context) {
        context.save(this);
    }
}
