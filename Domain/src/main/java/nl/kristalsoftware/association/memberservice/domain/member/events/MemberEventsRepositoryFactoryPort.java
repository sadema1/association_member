package nl.kristalsoftware.association.memberservice.domain.member.events;

public interface MemberEventsRepositoryFactoryPort {
    MemberEventsRepositoryPort createRepositoryAdapter();
}
