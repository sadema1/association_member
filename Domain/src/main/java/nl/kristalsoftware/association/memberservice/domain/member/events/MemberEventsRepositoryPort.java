package nl.kristalsoftware.association.memberservice.domain.member.events;

import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.ddd.domain.base.event.EventsRepositoryPort;

public interface MemberEventsRepositoryPort extends EventsRepositoryPort<Member> {
    void saveEvent(MemberSignedUp memberSignedUp);

    void saveEvent(MemberKindChanged memberKindChanged);

    void saveEvent(AddressAssignedToMember addressAssignedToMember);

    void saveEvent(AddressUnAssignedFromMember addressUnAssignedFromMember);
}
