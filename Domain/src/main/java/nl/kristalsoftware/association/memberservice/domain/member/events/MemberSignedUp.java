package nl.kristalsoftware.association.memberservice.domain.member.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEvent;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventSaving;

@DomainEvent
@Getter
@RequiredArgsConstructor(staticName = "of")
public class MemberSignedUp implements DomainEventLoading<Member>, DomainEventSaving<MemberRepository> {

    private final MemberReference memberReference;

    private final MemberName memberName;

    private final MemberBirthDate memberBirthDate;

    private final MemberKind memberKind;

    @Override
    public void load(Member aggregate) {
        aggregate.loadEventData(this);
    }

    @Override
    public void save(MemberRepository memberRepository) {
        memberRepository.save(this);
    }

}
