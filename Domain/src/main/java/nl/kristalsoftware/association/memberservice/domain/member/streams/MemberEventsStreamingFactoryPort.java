package nl.kristalsoftware.association.memberservice.domain.member.streams;

public interface MemberEventsStreamingFactoryPort {
    MemberEventsStreamingPort createStreamingAdapter();
}
