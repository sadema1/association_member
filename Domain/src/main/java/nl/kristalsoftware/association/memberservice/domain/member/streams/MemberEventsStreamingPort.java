package nl.kristalsoftware.association.memberservice.domain.member.streams;

import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;

public interface MemberEventsStreamingPort {
    void produceEvent(MemberSignedUp memberSignedUp);

    void produceEvent(Member member, MemberKindChanged memberKindChanged);

    void produceEvent(Member member, AddressAssignedToMember addressAssignedToMember);

    void produceEvent(Member member, AddressUnAssignedFromMember addressUnAssignedFromMember);
}
