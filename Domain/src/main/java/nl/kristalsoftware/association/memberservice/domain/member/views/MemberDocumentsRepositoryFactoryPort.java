package nl.kristalsoftware.association.memberservice.domain.member.views;

public interface MemberDocumentsRepositoryFactoryPort {
    MemberDocumentsRepositoryPort createRepositoryAdapter();
}
