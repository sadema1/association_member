package nl.kristalsoftware.association.memberservice.domain.member.views;

import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.ddd.domain.base.view.DocumentsRepositoryPort;

public interface MemberDocumentsRepositoryPort extends DocumentsRepositoryPort<Member> {
    void saveDocument(MemberSignedUp memberSignedUp);
    void saveDocument(MemberKindChanged memberKindChanged);

    void saveDocument(AddressAssignedToMember addressAssignedToMember);

    void saveDocument(AddressUnAssignedFromMember addressUnAssignedFromMember);
}
