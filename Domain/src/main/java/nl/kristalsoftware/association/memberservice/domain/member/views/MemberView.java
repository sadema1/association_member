package nl.kristalsoftware.association.memberservice.domain.member.views;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;

import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
public class MemberView {
    private MemberReference memberReference;
    private MemberName memberName;
    private MemberBirthDate memberBirthDate;
    private MemberKind memberKind;
    private List<Address> addresses;
}
