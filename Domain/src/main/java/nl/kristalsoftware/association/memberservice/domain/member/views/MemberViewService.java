package nl.kristalsoftware.association.memberservice.domain.member.views;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MemberViewService {

    private final MemberViewStorePort memberViewStorePort;

    public List<MemberView> getAllMembers() {
        return memberViewStorePort.getAllMembers();
    }

    public MemberView getMemberByReference(MemberReference memberReference) throws AggregateNotFoundException {
        return memberViewStorePort.getMemberByReference(memberReference);
    }
}
