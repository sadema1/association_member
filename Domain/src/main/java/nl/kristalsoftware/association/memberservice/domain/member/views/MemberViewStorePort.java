package nl.kristalsoftware.association.memberservice.domain.member.views;

import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;

import java.util.List;

public interface MemberViewStorePort {
    List<MemberView> getAllMembers();

    MemberView getMemberByReference(MemberReference memberReference) throws AggregateNotFoundException;
//    void save(List<BaseDomainEvent<Member>> eventList, Member aggregate);

}
