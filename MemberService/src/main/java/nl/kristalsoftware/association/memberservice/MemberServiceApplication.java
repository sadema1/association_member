package nl.kristalsoftware.association.memberservice;

import nl.kristalsoftware.association.memberservice.domain.member.attributes.Kind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.City;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Street;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.StreetNumber;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.ZipCode;
import nl.kristalsoftware.association.memberservice.domain.member.commands.MemberCommandService;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class MemberServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberServiceApplication.class, args);
    }

    @Profile("local")
    @Bean
    CommandLineRunner createSomeData(MemberCommandService memberCommandService) {
        return args -> {
                List<Address> addresses = List.of(Address.of(
                        AddressReference.of(""),
                        Street.of("Doornweg"),
                        StreetNumber.of("4"),
                        ZipCode.of("7300AA"),
                        City.of("Apeldoorn")
                ), Address.of(
                        AddressReference.of(""),
                        Street.of("Dennenlaan"),
                        StreetNumber.of("1"),
                        ZipCode.of("7315BB"),
                        City.of("Apeldoorn")
                ));
                MemberName memberName = MemberName.of("Wisse", "Adema");
                MemberBirthDate memberBirthDate = MemberBirthDate.of(LocalDate.of(2003, 9, 6));
                MemberKind memberKind = MemberKind.of(Kind.PLAYER);
                Optional<MemberReference> memberReference = memberCommandService.signUpMember(
                        memberName,
                        memberBirthDate,
                        memberKind
                );
            try {
                if (memberReference.isPresent()) {
                    memberCommandService.editMember(
                            memberReference.get(),
                            memberName,
                            memberBirthDate,
                            MemberKind.of(Kind.SUPPORTING_MEMBER),
                            addresses
                    );
                }
            } catch (AggregateNotFoundException e) {
                e.printStackTrace();
            }
        };
    }
}
