package nl.kristalsoftware.association.memberservice.eventstore;

import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.City;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Street;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.StreetNumber;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.ZipCode;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity(name = "AddressAssignedToMemberEvent")
public class AddressAssignedToMemberEntity extends MemberBaseEventEntity<AddressAssignedToMember> {

    private UUID addressReference;
    private String street;
    private String streetNumber;
    private String zipCode;
    private String city;

    private AddressAssignedToMemberEntity(
            UUID reference,
            String domainEventName,
            UUID addressReference,
            String street,
            String streetNumber,
            String zipCode,
            String city
    ) {
        super(reference, domainEventName);
        this.addressReference = addressReference;
        this.street = street;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        this.city = city;
    }

    public static AddressAssignedToMemberEntity of(AddressAssignedToMember addressAssignedToMember) {
        return new AddressAssignedToMemberEntity(
                addressAssignedToMember.getMemberReference().getValue(),
                addressAssignedToMember.getClass().getSimpleName(),
                addressAssignedToMember.getAddressReference().getValue(),
                addressAssignedToMember.getStreet().getValue(),
                addressAssignedToMember.getStreetNumber().getValue(),
                addressAssignedToMember.getZipCode().getValue(),
                addressAssignedToMember.getCity().getValue()
                );
    }

    @Transient
    @Override
    public AddressAssignedToMember getDomainEvent() {
        return AddressAssignedToMember.of(
                MemberReference.of(getReference()),
                AddressReference.of(addressReference),
                Street.of(street),
                StreetNumber.of(streetNumber),
                ZipCode.of(zipCode),
                City.of(city)
        );
    }
}
