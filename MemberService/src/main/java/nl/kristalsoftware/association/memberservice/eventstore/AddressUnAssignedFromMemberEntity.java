package nl.kristalsoftware.association.memberservice.eventstore;

import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;

import javax.persistence.Entity;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity(name = "AddressUnassignedFromMemberEvent")
public class AddressUnAssignedFromMemberEntity extends MemberBaseEventEntity<AddressUnAssignedFromMember> {

    private UUID addressReference;
    private AddressUnAssignedFromMemberEntity(
            UUID reference,
            String domainEventName,
            UUID addressReference
    ) {
        super(reference, domainEventName);
        this.addressReference = addressReference;
    }

    public static AddressUnAssignedFromMemberEntity of(AddressUnAssignedFromMember addressUnAssignedFromMember) {
        return new AddressUnAssignedFromMemberEntity(
                addressUnAssignedFromMember.getMemberReference().getValue(),
                addressUnAssignedFromMember.getClass().getSimpleName(),
                addressUnAssignedFromMember.getAddressReference().getValue()
                );
    }

    @Override
    public AddressUnAssignedFromMember getDomainEvent() {
        return AddressUnAssignedFromMember.of(
                MemberReference.of(getReference()),
                AddressReference.of(addressReference)
        );
    }
}
