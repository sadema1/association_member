package nl.kristalsoftware.association.memberservice.eventstore;


import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface MemberEventStoreRepository extends CrudRepository<MemberBaseEventEntity, Long> {

    Iterable<MemberBaseEventEntity<? extends DomainEventLoading<Member>>> findAllByReference(UUID value);

    Optional<MemberBaseEventEntity<?>> findFirstByReference(UUID value);

}
