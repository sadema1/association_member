package nl.kristalsoftware.association.memberservice.eventstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberEventsRepositoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class MemberEventsRepositoryAdapter implements MemberEventsRepositoryPort {

    private final MemberEventStoreRepository memberEventStoreRepository;

    @Override
    public void saveEvent(MemberSignedUp memberSignedUp) {
        memberEventStoreRepository.save(MemberSignedUpEventEntity.of(memberSignedUp));
    }

    @Override
    public void saveEvent(MemberKindChanged memberKindChanged) {
        memberEventStoreRepository.save(MemberKindChangedEventEntity.of(memberKindChanged));
    }

    @Override
    public void saveEvent(AddressAssignedToMember addressAssignedToMember) {
        memberEventStoreRepository.save(AddressAssignedToMemberEntity.of(addressAssignedToMember));
    }

    @Override
    public void saveEvent(AddressUnAssignedFromMember addressUnAssignedFromMember) {
        memberEventStoreRepository.save(AddressUnAssignedFromMemberEntity.of(addressUnAssignedFromMember));
    }

    @Override
    public List<DomainEventLoading<Member>> findAllDomainEventsByReference(Member aggregate) {
        List<DomainEventLoading<Member>> list = new ArrayList<>();
        for (MemberBaseEventEntity<? extends DomainEventLoading<Member>> it : memberEventStoreRepository.findAllByReference(aggregate.getReference().getValue())) {
            DomainEventLoading<Member> domainEvent = it.getDomainEvent();
            list.add(domainEvent);
        }
        return list;
    }
}
