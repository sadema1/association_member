package nl.kristalsoftware.association.memberservice.eventstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberEventsRepositoryFactoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberEventsRepositoryPort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class MemberEventsRepositoryFactoryAdapter implements MemberEventsRepositoryFactoryPort {

    private final MemberEventStoreRepository memberEventStoreRepository;

    @Override
    public MemberEventsRepositoryPort createRepositoryAdapter() {
        return new MemberEventsRepositoryAdapter(memberEventStoreRepository);
    }
}
