package nl.kristalsoftware.association.memberservice.eventstore;

import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.Kind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity(name = "MemberKindChangedEvent")
public class MemberKindChangedEventEntity extends MemberBaseEventEntity<MemberKindChanged> {
    @Enumerated(EnumType.STRING)
    private Kind kind;

    private MemberKindChangedEventEntity(UUID reference, String domainEventName, Kind kind) {
        super(reference, domainEventName);
        this.kind = kind;
    }

    public static MemberKindChangedEventEntity of(MemberKindChanged memberKindChanged) {
        return new MemberKindChangedEventEntity(
            memberKindChanged.getMemberReference().getValue(),
            memberKindChanged.getClass().getSimpleName(),
            memberKindChanged.getMemberKind().getValue()
        );
    }

    @Override
    public MemberKindChanged getDomainEvent() {
        return MemberKindChanged.of(
                MemberReference.of(getReference()),
                MemberKind.of(kind)
        );
    }
}
