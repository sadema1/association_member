package nl.kristalsoftware.association.memberservice.eventstore;

import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.Kind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.ddd.domain.base.type.TinyDateType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity(name = "MemberSignedUpEvent")
public class MemberSignedUpEventEntity extends MemberBaseEventEntity<MemberSignedUp> {

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    private Kind kind;

    private MemberSignedUpEventEntity(
            UUID reference,
            String domainEventName,
            String firstName,
            String lastName,
            LocalDate birthDate,
            Kind kind
    ) {
        super(reference, domainEventName);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.kind = kind;
    }

    public static MemberSignedUpEventEntity of(MemberSignedUp memberSignedUp) {
        return new MemberSignedUpEventEntity(
                memberSignedUp.getMemberReference().getValue(),
                memberSignedUp.getClass().getSimpleName(),
                memberSignedUp.getMemberName().getFirstName(),
                memberSignedUp.getMemberName().getLastName(),
                TinyDateType.getLocalDateFromMillis(memberSignedUp.getMemberBirthDate().getDateInMillis()),
                memberSignedUp.getMemberKind().getValue()
        );
    }


    @Override
    public MemberSignedUp getDomainEvent() {
        return MemberSignedUp.of(
                MemberReference.of(getReference()),
                MemberName.of(firstName, lastName),
                MemberBirthDate.of(birthDate),
                MemberKind.of(kind)
        );
    }

}
