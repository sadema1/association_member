package nl.kristalsoftware.association.memberservice.eventstream;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.association.memberservice.domain.member.streams.MemberEventsStreamingPort;
import nl.kristalsoftware.association.memberservice.eventstream.member.Address;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberData;
import nl.kristalsoftware.association.memberservice.eventstream.member.MemberEventData;
import nl.kristalsoftware.association.memberservice.eventstream.member.UUID;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class MemberEventsStreamingAdapter implements MemberEventsStreamingPort {

    private final MemberEventDataProducer memberEventDataProducer;

    @Override
    public void produceEvent(MemberSignedUp memberSignedUp) {
        MemberEventData memberEventData = MemberEventData.newBuilder()
                .setDomainEventName(memberSignedUp.getClass().getSimpleName())
                .setReference(UUID.newBuilder().setValue(memberSignedUp.getMemberReference().getValue().toString()))
                .setEventData(MemberData.newBuilder()
                        .setFirstName(memberSignedUp.getMemberName().getFirstName())
                        .setLastName(memberSignedUp.getMemberName().getLastName())
                        .setBirthDate(memberSignedUp.getMemberBirthDate().getDateInMillis())
                        .setKind(memberSignedUp.getMemberKind().getValue().name())
                        .build()
                )
                .build();
        memberEventDataProducer.produce(memberEventData);
    }

    @Override
    public void produceEvent(Member member, MemberKindChanged memberKindChanged) {
        MemberEventData memberEventData = MemberEventData.newBuilder()
                .setDomainEventName(memberKindChanged.getClass().getSimpleName())
                .setReference(UUID.newBuilder()
                        .setValue(memberKindChanged.getMemberReference().getValue().toString())
                        .build())
                .setEventData(MemberData.newBuilder()
                        .setKind(memberKindChanged.getMemberKind().getValue().name())
                        .build()
                )
                .setAggregateData(MemberData.newBuilder()
                        .setFirstName(member.getMemberName().getFirstName())
                        .setLastName(member.getMemberName().getLastName())
                        .setBirthDate(member.getMemberBirthDate().getDateInMillis())
                        .setKind(member.getMemberKind().getValue().name())
                        .addAllAddresses(getAddressList(member))
                        .build()
                )
                .build();
        memberEventDataProducer.produce(memberEventData);
    }

    @Override
    public void produceEvent(Member member, AddressAssignedToMember addressAssignedToMember) {
        MemberEventData memberEventData = MemberEventData.newBuilder()
                .setDomainEventName(addressAssignedToMember.getClass().getSimpleName())
                .setReference(UUID.newBuilder().setValue(addressAssignedToMember.getAddressReference().getValue().toString()))
                .setEventData(MemberData.newBuilder()
                        .addAllAddresses(List.of(Address.newBuilder()
                                        .setReference(UUID.newBuilder().setValue(addressAssignedToMember.getAddressReference().getValue().toString()).build())
                                        .setStreet(addressAssignedToMember.getStreet().getValue())
                                        .setStreetNumber(addressAssignedToMember.getStreetNumber().getValue())
                                        .setZipCode(addressAssignedToMember.getZipCode().getValue())
                                        .setCity(addressAssignedToMember.getCity().getValue())
                                        .build()
                                )
                        )
                        .build()
                )
                .setAggregateData(MemberData.newBuilder()
                        .setFirstName(member.getMemberName().getFirstName())
                        .setLastName(member.getMemberName().getLastName())
                        .setBirthDate(member.getMemberBirthDate().getDateInMillis())
                        .setKind(member.getMemberKind().getValue().name())
                        .addAllAddresses(getAddressList(member))
                        .build()
                )
                .build();
        memberEventDataProducer.produce(memberEventData);
    }

    @Override
    public void produceEvent(Member member, AddressUnAssignedFromMember addressUnAssignedFromMember) {
        MemberEventData memberEventData = MemberEventData.newBuilder()
                .setDomainEventName(addressUnAssignedFromMember.getClass().getSimpleName())
                .setReference(UUID.newBuilder().setValue(addressUnAssignedFromMember.getAddressReference().getValue().toString()).build())
                .setEventData(MemberData.newBuilder()
                        .addAllAddresses(List.of(Address.newBuilder()
                                        .setReference(UUID.newBuilder().setValue(addressUnAssignedFromMember.getAddressReference().getValue().toString()).build())
                                        .build()
                                )
                        )
                        .build()
                )
                .setAggregateData(MemberData.newBuilder()
                        .setFirstName(member.getMemberName().getFirstName())
                        .setLastName(member.getMemberName().getLastName())
                        .setBirthDate(member.getMemberBirthDate().getDateInMillis())
                        .setKind(member.getMemberKind().getValue().name())
                        .addAllAddresses(getAddressList(member))
                        .build()
                )
                .build();
        memberEventDataProducer.produce(memberEventData);
    }

    private static List<Address> getAddressList(Member member) {
        return member.getAddresses().stream()
                .map(it -> Address.newBuilder()
                        .setReference(UUID.newBuilder().setValue(it.getReference().getValue().toString()).build())
                        .setStreet(it.getStreet().getValue())
                        .setStreetNumber(it.getStreetNumber().getValue())
                        .setZipCode(it.getZipCode().getValue())
                        .setCity(it.getCity().getValue())
                        .build()
                )
                .toList();
    }
}
