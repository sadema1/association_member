package nl.kristalsoftware.association.memberservice.eventstream;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.streams.MemberEventsStreamingFactoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.streams.MemberEventsStreamingPort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class MemberEventsStreamingFactoryAdapter implements MemberEventsStreamingFactoryPort {

    private final MemberEventDataProducer memberEventDataProducer;

    @Override
    public MemberEventsStreamingPort createStreamingAdapter() {
        return new MemberEventsStreamingAdapter(memberEventDataProducer);
    }
}
