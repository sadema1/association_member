package nl.kristalsoftware.association.memberservice.rest.command;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.commands.MemberCommandService;
import nl.kristalsoftware.association.memberservice.rest.view.AddressBody;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/members", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Member commands", description = "Endpoints for updating members")
public class MemberCommandController {

    private final MemberCommandService memberCommandService;

    @PostMapping
    @Operation(
            summary = "Sign Up member",
            description = "Sign Up member",
            tags = "Member commands",
            responses = {
                    @ApiResponse(description = "Created", responseCode = "201"),
                    @ApiResponse(description = "Unprocessable Entity", responseCode = "422"),
            }
    )
    public ResponseEntity<Void> signUpMember(@RequestBody MemberSignUpRequestBody memberSignUpRequestBody) {
        Optional<MemberReference> memberReference = memberCommandService.signUpMember(
                MemberName.of(memberSignUpRequestBody.getFirstName(), memberSignUpRequestBody.getLastName()),
                MemberBirthDate.of(memberSignUpRequestBody.getBirthDate()),
                MemberKind.of(memberSignUpRequestBody.getKind())
        );
        if (memberReference.isPresent()) {
            return ResponseEntity.created(URI.create("/members/" + memberReference.get().getValue().toString())).build();
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    @PutMapping(value = "/{memberReference}")
    @Operation(
            summary = "Edit member by reference",
            description = "Edit member by reference",
            tags = "Member commands",
            responses = {
                    @ApiResponse(description = "No Content", responseCode = "204"),
                    @ApiResponse(description = "Not Found", responseCode = "404"),
            }
    )
    public ResponseEntity<Void> editMember(
            @NotNull @PathVariable String memberReference, @RequestBody MemberEditRequestBody memberEditRequestBody) {
        try {
            memberCommandService.editMember(
                    MemberReference.of(memberReference),
                    MemberName.of(memberEditRequestBody.getFirstName(), memberEditRequestBody.getLastName()),
                    MemberBirthDate.of(memberEditRequestBody.getBirthDate()),
                    MemberKind.of(memberEditRequestBody.getKind()),
                    memberEditRequestBody.getAddresses().stream()
                            .map(AddressBody::convertToDomain)
                            .toList()
            );
        } catch (AggregateNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
