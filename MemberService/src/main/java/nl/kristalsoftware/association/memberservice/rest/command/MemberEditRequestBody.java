package nl.kristalsoftware.association.memberservice.rest.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.rest.view.AddressBody;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class MemberEditRequestBody {

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private Long birthDate;
    private String kind = "PLAYER";
    private List<AddressBody> addresses = new ArrayList<>();

}
