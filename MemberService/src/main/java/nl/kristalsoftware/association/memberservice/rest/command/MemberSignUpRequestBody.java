package nl.kristalsoftware.association.memberservice.rest.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class MemberSignUpRequestBody {

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private Long birthDate;
    private String kind = "PLAYER";

}
