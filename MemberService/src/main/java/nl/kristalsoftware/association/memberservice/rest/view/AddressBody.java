package nl.kristalsoftware.association.memberservice.rest.view;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.City;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Street;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.StreetNumber;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.ZipCode;

import java.util.UUID;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class AddressBody {
    private UUID addressReference;
    private String street;
    private String streetNumber;
    private String zipCode;
    private String city;

    public Address convertToDomain() {
        return Address.of(
                AddressReference.of(addressReference),
                Street.of(street),
                StreetNumber.of(streetNumber),
                ZipCode.of(zipCode),
                City.of(city)
        );
    }

}
