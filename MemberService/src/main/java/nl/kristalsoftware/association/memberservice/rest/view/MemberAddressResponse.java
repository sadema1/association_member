package nl.kristalsoftware.association.memberservice.rest.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class MemberAddressResponse {
    private UUID reference;
    private String street;
    private String streetNumber;
    private String zipCode;
    private String city;

    public static MemberAddressResponse of(Address address) {
        return new MemberAddressResponse(
                address.getAddressReference().getValue(),
                address.getStreet().getValue(),
                address.getStreetNumber().getValue(),
                address.getZipCode().getValue(),
                address.getCity().getValue()
        );
    }
}
