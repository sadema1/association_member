package nl.kristalsoftware.association.memberservice.rest.view;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.Kind;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberView;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;
import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MemberResponseBody {

    private UUID reference;
    private String firstName;
    private String lastName;
    private String birthDate;
    @Enumerated(EnumType.STRING)
    private Kind kind;
    private List<MemberAddressResponse> addresses;

    public static MemberResponseBody of(MemberView memberView) {
        return new MemberResponseBody(
                memberView.getMemberReference().getValue(),
                memberView.getMemberName().getFirstName(),
                memberView.getMemberName().getLastName(),
                memberView.getMemberBirthDate().getValue().toString(),
                memberView.getMemberKind().getValue(),
                memberView.getAddresses().stream()
                        .map(it -> MemberAddressResponse.of(it))
                        .toList()
        );
    }

}
