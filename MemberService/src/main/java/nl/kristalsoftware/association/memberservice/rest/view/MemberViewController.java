package nl.kristalsoftware.association.memberservice.rest.view;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberView;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberViewService;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/members", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Members view", description = "Endpoints for retrieving members")
public class MemberViewController {

    private final MemberViewService memberViewService;

    @GetMapping
    @Operation(
            summary = "Get all members",
            description = "Get all members",
            tags = "Members view",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
            }
    )
    public List<MemberResponseBody> getAllMembers() {
        List<MemberView> memberList = memberViewService.getAllMembers();
        return memberList.stream()
                .map(it -> MemberResponseBody.of(it))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{memberReference}")
    @Operation(
            summary = "Get member by reference",
            description = "Get member by reference",
            tags = "Members view",
            responses = {
                    @ApiResponse(description = "OK", responseCode = "200"),
                    @ApiResponse(description = "Not Found", responseCode = "404"),
            }
    )
    public ResponseEntity<MemberResponseBody> getMemberByReference(@PathVariable String memberReference) {
        try {
            return ResponseEntity.ok(MemberResponseBody.of(memberViewService.getMemberByReference(MemberReference.of(memberReference))));
        } catch (AggregateNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
