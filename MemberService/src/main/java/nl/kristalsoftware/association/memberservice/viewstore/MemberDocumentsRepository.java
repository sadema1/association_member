package nl.kristalsoftware.association.memberservice.viewstore;

import nl.kristalsoftware.association.memberservice.viewstore.document.MemberDocument;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
import java.util.UUID;

public interface MemberDocumentsRepository extends MongoRepository<MemberDocument, ObjectId> {
    Optional<MemberDocument> findByReference(UUID value);

}

