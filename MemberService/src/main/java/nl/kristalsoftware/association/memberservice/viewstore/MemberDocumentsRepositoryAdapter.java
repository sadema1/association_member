package nl.kristalsoftware.association.memberservice.viewstore;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressAssignedToMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.AddressUnAssignedFromMember;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberKindChanged;
import nl.kristalsoftware.association.memberservice.domain.member.events.MemberSignedUp;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberDocumentsRepositoryPort;
import nl.kristalsoftware.association.memberservice.viewstore.document.MemberDocument;
import nl.kristalsoftware.association.memberservice.viewstore.document.MemberDocumentAddress;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class MemberDocumentsRepositoryAdapter implements MemberDocumentsRepositoryPort {

    private final MemberDocumentsRepository memberDocumentsRepository;
    private MemberDocument memberDocument = new MemberDocument();

    @Override
    public void createDocument(Member aggregate) {
        Optional<MemberDocument> result = memberDocumentsRepository.findByReference(aggregate.getReference().getValue());
        memberDocument = result.orElseThrow(() -> new IllegalStateException(String.format("Document with reference %s not found in viewstore!", aggregate.getReference().getValue())));
    }

    @Override
    public void saveDocument(MemberSignedUp memberSignedUp) {
        memberDocument = MemberDocument.of(
                memberSignedUp.getMemberReference().getValue(),
                memberSignedUp.getMemberName().getFirstName(),
                memberSignedUp.getMemberName().getLastName(),
                memberSignedUp.getMemberBirthDate().getDateInMillis(),
                memberSignedUp.getMemberKind().getValue().name()
        );
    }

    @Override
    public void saveDocument(MemberKindChanged memberKindChanged) {
        memberDocument.setKind(memberKindChanged.getMemberKind().getValue().name());
    }

    @Override
    public void saveDocument(AddressAssignedToMember addressAssignedToMember) {
        memberDocument.getAddresses().add(MemberDocumentAddress.of(
                addressAssignedToMember.getAddressReference().getValue(),
                addressAssignedToMember.getStreet().getValue(),
                addressAssignedToMember.getStreetNumber().getValue(),
                addressAssignedToMember.getZipCode().getValue(),
                addressAssignedToMember.getCity().getValue()
        ));
    }

    @Override
    public void saveDocument(AddressUnAssignedFromMember addressUnAssignedFromMember) {
        memberDocument.getAddresses().stream()
                .filter(it -> it.getReference().equals(addressUnAssignedFromMember.getAddressReference().getValue()))
                .findAny()
                .ifPresent(it -> {
                    memberDocument.getAddresses().remove(it);
                });
    }

    @Override
    public void saveDocument() {
        memberDocumentsRepository.save(memberDocument);
    }

}
