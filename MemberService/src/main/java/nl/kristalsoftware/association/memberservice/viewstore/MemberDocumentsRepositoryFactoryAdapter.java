package nl.kristalsoftware.association.memberservice.viewstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberDocumentsRepositoryFactoryPort;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberDocumentsRepositoryPort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class MemberDocumentsRepositoryFactoryAdapter implements MemberDocumentsRepositoryFactoryPort {

    private final MemberDocumentsRepository memberDocumentsRepository;

    @Override
    public MemberDocumentsRepositoryPort createRepositoryAdapter() {
        return new MemberDocumentsRepositoryAdapter(memberDocumentsRepository);
    }
}
