package nl.kristalsoftware.association.memberservice.viewstore;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberView;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberViewStorePort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class MemberViewStoreAdapter implements MemberViewStorePort {

    private final MemberDocumentsRepository memberDocumentsRepository;

    @Override
    public List<MemberView> getAllMembers() {
        return memberDocumentsRepository.findAll().stream()
                .map(it -> it.toMemberView())
                .toList();
    }

    @Override
    public MemberView getMemberByReference(MemberReference memberReference) throws AggregateNotFoundException {
        return memberDocumentsRepository.findByReference(memberReference.getValue())
                .map(it -> it.toMemberView()
                )
                .orElseThrow(() -> new AggregateNotFoundException(String.format("Member with reference: %s not found!", memberReference.getValue())));
    }

}
