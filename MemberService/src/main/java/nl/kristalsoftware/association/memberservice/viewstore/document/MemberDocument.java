package nl.kristalsoftware.association.memberservice.viewstore.document;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberBirthDate;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberKind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberName;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.domain.member.views.MemberView;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@Document
public class MemberDocument {

    @Setter(AccessLevel.NONE)
    @Id
    private ObjectId _id;
    private UUID reference;
    private String firstName;
    private String lastName;
    private Long birthDate;
    private String kind;
    private List<MemberDocumentAddress> addresses;
    @Version
    private Long version;

    private MemberDocument(UUID reference, String firstName, String lastName, Long birthDate, String kind) {
        this.reference = reference;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.kind = kind;
        this.addresses = new ArrayList<>();
    }

    public static MemberDocument of(UUID reference, String firstName, String lastName, Long birthDate, String kind) {
        return new MemberDocument(reference, firstName, lastName, birthDate, kind);
    }

    public MemberView toMemberView() {
        return MemberView.of(
                MemberReference.of(reference),
                MemberName.of(firstName, lastName),
                MemberBirthDate.of(birthDate),
                MemberKind.of(kind),
                addresses.stream()
                        .map(it -> it.toAddress())
                        .toList()
        );
    }
}
