package nl.kristalsoftware.association.memberservice.viewstore.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Address;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.AddressReference;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.City;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.Street;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.StreetNumber;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.address.ZipCode;

import java.util.UUID;

@Data
@AllArgsConstructor(staticName = "of")
public class MemberDocumentAddress {
    private UUID reference;
    private String street;
    private String streetNumber;
    private String zipCode;
    private String city;

    public Address toAddress() {
        return Address.of(
                AddressReference.of(reference),
                Street.of(street),
                StreetNumber.of(streetNumber),
                ZipCode.of(zipCode),
                City.of(city)
        );
    }
}
