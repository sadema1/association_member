package nl.kristalsoftware.association.memberservice;

import nl.kristalsoftware.association.memberservice.base.AbstractContainerBasedUseCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class MemberServiceApplicationTests extends AbstractContainerBasedUseCase {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() {
    }

    @Test
    void healty() throws Exception {
        ResultActions resultActions = performGetRequest("/actuator/health");
        resultActions.andExpect(status().isOk());
    }

    private ResultActions performGetRequest(String url) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get(url));
    }
}
