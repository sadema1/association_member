package nl.kristalsoftware.association.memberservice.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.lifecycle.Startables;
import org.testcontainers.utility.DockerImageName;

import java.text.SimpleDateFormat;
import java.util.Date;

//@SpringBootTest(
//        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
//        properties = {
//                "spring.datasource.url=jdbc:tc:mariadb:///member"
//        }
//)
@Slf4j
public abstract class AbstractContainerBasedUseCase {

    static final MariaDBContainer mariadb = new MariaDBContainer<>("mariadb:10.9")
            .withDatabaseName("member");
    static final MongoDBContainer mongodb = new MongoDBContainer(DockerImageName.parse("mongo:6.0"));
    static final KafkaContainer kafka = new KafkaContainer("6.1.9");

//    static {
//        mariadb = new MariaDBContainer<>("mariadb:10.9")
//                .withDatabaseName("member");
//        Startables.deepStart(mariadb).join();
//        mariadb.start();
//    }

    @DynamicPropertySource
    static void configureMariaDbTestProperties(DynamicPropertyRegistry registry) {
        Startables.deepStart(mariadb, mongodb, kafka).join();

        registry.add("spring.datasource.url", mariadb::getJdbcUrl);
        registry.add("spring.datasource.username", mariadb::getUsername);
        registry.add("spring.datasource.password", mariadb::getPassword);

        registry.add("spring.data.mongodb.host", mongodb::getHost);
        registry.add("spring.data.mongodb.port", mongodb::getFirstMappedPort);
//        registry.add("spring.data.mongodb.username", mongodb::);
//        registry.add("spring.data.mongodb.host", mongodb::getHost);

        registry.add("spring.kafka.bootstrap-servers", kafka::getBootstrapServers);
        log.info("======== {} {} {} ========", mariadb.getJdbcUrl(), mariadb.getUsername(), mariadb.getPassword());
    }
    protected ObjectMapper objectMapper;

//    @LocalServerPort
//    protected int localServerPort;

    @Autowired
    void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String toJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String millisToStringDate(String dateFormat, long millis) {
        Date date = new Date(millis);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        return simpleDateFormat.format(date);
    }

    public String getReferenceFromUrl(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }

}
