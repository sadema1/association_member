package nl.kristalsoftware.association.memberservice.base;

import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepositoryService;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.Member;
import nl.kristalsoftware.association.memberservice.domain.member.aggregate.MemberAggregateFactory;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.rest.command.MemberEditRequestBody;
import nl.kristalsoftware.association.memberservice.rest.command.MemberSignUpRequestBody;
import nl.kristalsoftware.association.memberservice.setup.member.MemberRequestBodyTestFactory;
import nl.kristalsoftware.association.memberservice.setup.member.PredefinedTestMember;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class AbstractMemberUseCase extends AbstractContainerBasedUseCase {

    private MockMvc mockMvc;
    protected PredefinedTestMember.PredefinedTestMemberBuilder memberNiekSchokmanBuilder;
//    protected String memberNiekSchokmanUrl;
//    protected String memberNiekSchokmanReference;
    private MemberAggregateFactory memberAggregateFactory;
    private MemberRepositoryService memberRepositoryService;

    @Autowired
    public void setMockMvc(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    @Autowired
    public void setMemberAggregateFactory(MemberAggregateFactory memberAggregateFactory) {
        this.memberAggregateFactory = memberAggregateFactory;
    }

    @Autowired
    void setMemberRepositoryService(MemberRepositoryService memberRepositoryService) {
        this.memberRepositoryService = memberRepositoryService;
    }

    protected String createMember(PredefinedTestMember member) throws Exception {
        memberNiekSchokmanBuilder = PredefinedTestMember.memberNiekSchokmanBuilder();
        MemberSignUpRequestBody memberSignUpRequestBody =
                MemberRequestBodyTestFactory.memberSignUpRequestBody(memberNiekSchokmanBuilder.build());
        ResultActions signUpMemberResult = performPostRequest(memberSignUpRequestBody);
        String memberNiekSchokmanUrl = signUpMemberResult.andReturn().getResponse().getHeader("Location");
        member.setReference(UUID.fromString(getReferenceFromUrl(memberNiekSchokmanUrl)));
        return memberNiekSchokmanUrl;
//        memberNiekSchokmanReference = memberNiekSchokmanUrl.substring(memberNiekSchokmanUrl.lastIndexOf('/') + 1);
    }

    protected void editPredifinedTestMember(PredefinedTestMember predefinedTestMember, String testMemberReference) throws Exception {
        MemberEditRequestBody memberEditRequestBody =
                MemberRequestBodyTestFactory.memberEditRequestBody(predefinedTestMember);
        ResultActions editMemberResult = performPutRequest(testMemberReference, memberEditRequestBody);
        editMemberResult
                .andExpect(status().isNoContent());
    }

    private ResultActions performPostRequest(MemberSignUpRequestBody memberSignUpRequestBody) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.post("/members")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(memberSignUpRequestBody)));
    }

    private ResultActions performPutRequest(String memberReference, MemberEditRequestBody memberEditRequestBody) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.put("/members/{memberReference}", memberReference)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(memberEditRequestBody)));
    }

    protected ResultActions performGetRequest(String url) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get(url));
    }

    @NotNull
    protected MemberRepository createMemberRepository(MemberReference memberReference) {
        MemberRepository memberRepository = MemberRepository.of(
                memberReference,
                memberAggregateFactory,
                memberRepositoryService);
        return memberRepository;
    }

    protected void checkPredefinedMember(ResultActions resultActions, PredefinedTestMember testMember) throws Exception {
        // Assert
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reference").value(equalTo(testMember.getReference().toString())))
                .andExpect(jsonPath("$.firstName").value(equalTo(testMember.getFirstName())))
                .andExpect(jsonPath("$.lastName").value(equalTo(testMember.getLastName())))
                .andExpect(jsonPath("$.birthDate").value(equalTo(millisToStringDate("yyyy-MM-dd", testMember.getBirthDate()))))
                .andExpect(jsonPath("$.kind").value(equalTo(testMember.getKind())));
    }

    protected void checkLoadedAggregate(Member aggregate, PredefinedTestMember testMember) {
        assertThat(aggregate.getReference().getValue()).isNotNull();
        assertThat(aggregate.getMemberName().getFirstName()).isEqualTo(testMember.getFirstName());
        assertThat(aggregate.getMemberName().getLastName()).isEqualTo(testMember.getLastName());
        assertThat(aggregate.getMemberBirthDate().getDateInMillis()).isEqualTo(testMember.getBirthDate());
        assertThat(aggregate.getMemberKind().getValue().name()).isEqualTo(testMember.getKind());
    }

}
