package nl.kristalsoftware.association.memberservice.setup.member;

import nl.kristalsoftware.association.memberservice.rest.command.MemberEditRequestBody;
import nl.kristalsoftware.association.memberservice.rest.command.MemberSignUpRequestBody;
import nl.kristalsoftware.association.memberservice.rest.view.AddressBody;

public class MemberRequestBodyTestFactory {

    public static MemberSignUpRequestBody memberSignUpRequestBody(PredefinedTestMember predefinedTestMember) {
        return MemberSignUpRequestBody.of(
                predefinedTestMember.getFirstName(),
                predefinedTestMember.getLastName(),
                predefinedTestMember.getBirthDate(),
                predefinedTestMember.getKind()
        );
    }

    public static MemberEditRequestBody memberEditRequestBody(PredefinedTestMember predefinedTestMember) {
        return MemberEditRequestBody.of(
                predefinedTestMember.getFirstName(),
                predefinedTestMember.getLastName(),
                predefinedTestMember.getBirthDate(),
                predefinedTestMember.getKind(),
                predefinedTestMember.getAddresses().stream()
                        .map(it -> AddressBody.of(
                                it.getAddressReference(),
                                it.getStreet(),
                                it.getStreetNumber(),
                                it.getZipCode(),
                                it.getCity())
                        ).toList()

        );
    }

}
