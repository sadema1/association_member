package nl.kristalsoftware.association.memberservice.setup.member;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import nl.kristalsoftware.association.memberservice.setup.member.address.PredefinedTestAddress;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
public class PredefinedTestMember {

    private UUID reference;
    private String firstName;
    private String lastName;
    private long birthDate;
    private String kind;
    private List<PredefinedTestAddress> addresses;

    public static PredefinedTestMemberBuilder memberNiekSchokmanBuilder() {
        return PredefinedTestMember.builder()
                .firstName("Niek")
                .lastName("Schokman")
                .birthDate(166143600000L)
                .kind("PLAYER")
                .addresses(List.of());
    }

    public static PredefinedTestMemberBuilder memberDaphneSchellekensBuilder() {
        return PredefinedTestMember.builder()
                .firstName("Daphne")
                .lastName("Schellekens")
                .birthDate(-1177463972000L)
                .kind("PLAYER")
                .addresses(List.of());
    }

    public static PredefinedTestMemberBuilder memberOlafLamotteBuilder() {
        return PredefinedTestMember.builder()
                .firstName("Olaf")
                .lastName("Lamotte")
                .birthDate(545349600000L)
                .kind("PLAYER")
                .addresses(List.of());
    }

}
