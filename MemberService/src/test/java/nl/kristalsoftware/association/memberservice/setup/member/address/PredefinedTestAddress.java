package nl.kristalsoftware.association.memberservice.setup.member.address;

import lombok.Builder;
import lombok.Getter;
import nl.kristalsoftware.association.memberservice.rest.view.AddressBody;

import java.util.UUID;

@Getter
@Builder
public class PredefinedTestAddress {

    private UUID addressReference;
    private String street;
    private String streetNumber;
    private String zipCode;
    private String city;

    public static PredefinedTestAddress.PredefinedTestAddressBuilder addressValeriewegBuilder() {
        return PredefinedTestAddress.builder()
                .street("Valerieweg")
                .streetNumber("19")
                .zipCode("4248 BR")
                .city("Terband");
    }

    public static PredefinedTestAddress.PredefinedTestAddressBuilder addressFelinelaanBuilder() {
        return PredefinedTestAddress.builder()
                .street("Felinelaan")
                .streetNumber("282")
                .zipCode("7846 AK")
                .city("Oldeberkoop");
    }

    public static PredefinedTestAddress.PredefinedTestAddressBuilder addressNoorhofBuilder() {
        return PredefinedTestAddress.builder()
                .street("Noorhof")
                .streetNumber("9")
                .zipCode("5796OU")
                .city("Anerveen");
    }

    public static PredefinedTestAddress.PredefinedTestAddressBuilder addressBerendbaanBuilder() {
        return PredefinedTestAddress.builder()
                .street("Berendbaan")
                .streetNumber("591")
                .zipCode("8127 HK")
                .city("Eesterga");
    }

    public AddressBody fromPredefinedTestAddress(PredefinedTestAddress predefinedTestAddress) {
        return AddressBody.of(
                predefinedTestAddress.addressReference,
                predefinedTestAddress.getStreet(),
                predefinedTestAddress.streetNumber,
                predefinedTestAddress.zipCode,
                predefinedTestAddress.getCity()
        );
    }

}
