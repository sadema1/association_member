package nl.kristalsoftware.association.memberservice.usecase;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.base.AbstractMemberUseCase;
import nl.kristalsoftware.association.memberservice.rest.view.MemberResponseBody;
import nl.kristalsoftware.association.memberservice.setup.member.PredefinedTestMember;
import nl.kristalsoftware.association.memberservice.setup.member.address.PredefinedTestAddress;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Disabled
@Slf4j
@AutoConfigureMockMvc
@SpringBootTest
public class MemberAddressUseCaseTest extends AbstractMemberUseCase {

    private PredefinedTestAddress.PredefinedTestAddressBuilder addressValeriewegBuilder;
    private PredefinedTestAddress.PredefinedTestAddressBuilder addressFelinelaanBuilder;
    private PredefinedTestAddress.PredefinedTestAddressBuilder addressNoorhofBuilder;
    private PredefinedTestAddress.PredefinedTestAddressBuilder addressBerendbaanBuilder;
    private PredefinedTestMember niekSchokman;
    private String niekSchokmanUrl;

    @BeforeEach
    void init() throws Exception {
        niekSchokman = PredefinedTestMember.memberNiekSchokmanBuilder().build();
        super.createMember(niekSchokman);
        addressValeriewegBuilder = PredefinedTestAddress.addressValeriewegBuilder();
        addressFelinelaanBuilder = PredefinedTestAddress.addressFelinelaanBuilder();
        // Arrange
        niekSchokman.setAddresses(List.of(addressValeriewegBuilder.build(), addressFelinelaanBuilder.build()));
//        niekSchokman = memberNiekSchokmanBuilder
//                .addresses(List.of(addressValeriewegBuilder.build(), addressFelinelaanBuilder.build()))
//                .build();
        editPredifinedTestMember(niekSchokman, niekSchokman.getReference().toString());

        // Act
        ResultActions resultActions = performGetRequest(niekSchokmanUrl);
        String responseAsString = resultActions.andReturn().getResponse().getContentAsString();
        log.info(responseAsString);
        MemberResponseBody memberResponseBody = objectMapper.readValue(responseAsString, MemberResponseBody.class);
        addressValeriewegBuilder.addressReference(memberResponseBody.getAddresses().get(0).getReference());
        addressFelinelaanBuilder.addressReference(memberResponseBody.getAddresses().get(1).getReference());
    }


    @Test
    void testAddressesOfNiekSchokman() throws Exception {
        // Act
        ResultActions resultActions = performGetRequest(niekSchokmanUrl);
        // Assert
        checkPredefinedMember(resultActions, niekSchokman);
        checkAddresses(resultActions, niekSchokman.getAddresses());
    }

    @Test
    void testAddTwoAddressesToNiekSchokman() throws Exception {
        // Arrange
        addressNoorhofBuilder = PredefinedTestAddress.addressNoorhofBuilder();
        addressBerendbaanBuilder = PredefinedTestAddress.addressBerendbaanBuilder();
        List<PredefinedTestAddress> addresses = List.of(
                addressValeriewegBuilder.build(),
                addressFelinelaanBuilder.build(),
                addressNoorhofBuilder.build(),
                addressBerendbaanBuilder.build()
        );
        niekSchokman = memberNiekSchokmanBuilder
                .addresses(addresses)
                .build();
        editPredifinedTestMember(niekSchokman, niekSchokman.getReference().toString());
        // Act
        ResultActions resultActions = performGetRequest(niekSchokmanUrl);
        // Assert
        resultActions
                .andExpect(jsonPath("$.addresses.size()").value(niekSchokman.getAddresses().size()));
        checkPredefinedMember(resultActions, niekSchokman);
        checkAddresses(resultActions, niekSchokman.getAddresses());
    }

    @Test
    void testRemoveValeriewegOfNiekSchokman() throws Exception {
        // Arrange
        List<PredefinedTestAddress> addresses = List.of(
                addressFelinelaanBuilder.build()
        );
        niekSchokman = memberNiekSchokmanBuilder
                .addresses(addresses)
                .build();
        editPredifinedTestMember(niekSchokman, niekSchokman.getReference().toString());
        // Act
        ResultActions resultActions = performGetRequest(niekSchokmanUrl);
        // Assert
        resultActions
                .andExpect(jsonPath("$.addresses.size()").value(niekSchokman.getAddresses().size()));
        checkPredefinedMember(resultActions, niekSchokman);
        checkAddresses(resultActions, niekSchokman.getAddresses());
    }

    @Test
    void testChangeStreetnumberOfValeriewegOfNiekSchokman() throws Exception {
        // Arrange
        List<PredefinedTestAddress> addresses = List.of(
                addressValeriewegBuilder
                        .city("Oranjewoud")
                        .build(),
                addressFelinelaanBuilder.build()
        );
        niekSchokman = memberNiekSchokmanBuilder
                .addresses(addresses)
                .build();
        editPredifinedTestMember(niekSchokman, niekSchokman.getReference().toString());
        // Act
        ResultActions resultActions = performGetRequest(niekSchokmanUrl);
        // Assert
        resultActions
                .andExpect(jsonPath("$.addresses.size()").value(niekSchokman.getAddresses().size()));
        checkPredefinedMember(resultActions, niekSchokman);
        checkAddresses(resultActions, niekSchokman.getAddresses());
    }

    private void checkAddresses(ResultActions resultActions, List<PredefinedTestAddress> addresses) throws Exception {
        for (int i = 0; i < addresses.size(); i++) {
            resultActions
                    .andExpect(jsonPath(String.format("$.addresses[%d].reference", i)).exists())
                    .andExpect(jsonPath(String.format("$.addresses[%d].street", i)).value(equalTo(addresses.get(i).getStreet())))
                    .andExpect(jsonPath(String.format("$.addresses[%d].streetNumber", i)).value(equalTo(addresses.get(i).getStreetNumber())))
                    .andExpect(jsonPath(String.format("$.addresses[%d].zipCode", i)).value(equalTo(addresses.get(i).getZipCode())))
                    .andExpect(jsonPath(String.format("$.addresses[%d].city", i)).value(equalTo(addresses.get(i).getCity())));
        }
    }

}
