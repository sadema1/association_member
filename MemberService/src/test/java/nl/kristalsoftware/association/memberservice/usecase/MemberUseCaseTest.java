package nl.kristalsoftware.association.memberservice.usecase;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.memberservice.base.AbstractMemberUseCase;
import nl.kristalsoftware.association.memberservice.domain.member.MemberRepository;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.Kind;
import nl.kristalsoftware.association.memberservice.domain.member.attributes.MemberReference;
import nl.kristalsoftware.association.memberservice.setup.member.PredefinedTestMember;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.ResultActions;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Slf4j
@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest
public class MemberUseCaseTest extends AbstractMemberUseCase {

    private PredefinedTestMember niekSchokman;
    private String niekSchokmanUrl;

//    @Container
//    public static MariaDBContainer mariadb = new MariaDBContainer<>("mariadb:10.9")
//            .withDatabaseName("member")
//            .withReuse(true);

    public static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:latest"));

//    static TcpProxy tcpProxy;

//    @DynamicPropertySource
//    static void configureMariaDbTestProperties(DynamicPropertyRegistry registry) {
////        Startables.deepStart(mariadb).join();
//
//        registry.add("spring.datasource.url", mariadb.getJdbcUrl());
//        registry.add("spring.datasource.username", mariadb::getUsername);
//        registry.add("spring.datasource.password", mariadb::getPassword);
//
////        registry.add("spring.kafka.bootstrap-servers", kafka::getBootstrapServers);
//        log.info("======== {} {} {} ========", mariadb.getJdbcUrl(), mariadb.getUsername(), mariadb.getPassword());
//    }

    @BeforeEach
    void init() throws Exception {
        niekSchokman = PredefinedTestMember.memberNiekSchokmanBuilder().build();
        niekSchokmanUrl = super.createMember(niekSchokman);
    }

    @Test
    void testMemberSignedUp() throws Exception {
        // Arrange
        // Act
        ResultActions resultActions = performGetRequest(niekSchokmanUrl);
        // Assert
//        PredefinedTestMember memberNiekSchokman = memberNiekSchokmanBuilder.build();
        checkPredefinedMember(resultActions, niekSchokman);
        MemberRepository memberRepository = createMemberRepository(MemberReference.of(niekSchokman.getReference()));
        checkLoadedAggregate(memberRepository.getAggregate(), niekSchokman);
    }

    @Test
    void testMemberKindChanged() throws Exception {
        // Arrange
        niekSchokman.setKind(Kind.SUPPORTING_MEMBER.name());
//        PredefinedTestMember memberNiekSchokman = memberNiekSchokmanBuilder
//                .kind(Kind.SUPPORTING_MEMBER.name())
//                .build();
        editPredifinedTestMember(niekSchokman, niekSchokman.getReference().toString());
        // Act
        ResultActions resultActions = performGetRequest(niekSchokmanUrl);
        // Assert
        checkPredefinedMember(resultActions, niekSchokman);
        resultActions.andExpect(jsonPath("$.addresses").isEmpty());
    }

}
