# MemberService

## Kafka
Start Zookeeper and Kafka broker
```shell
cd ..
docker-compose up -d
```

## Owner of topics
The MemberService is the owner of topics:
- public.association.memberservice.member


## Create the topic
```shell
./topics-create.sh public.association.memberservice.member
```

## Swagger
```shell
http://localhost:9821/swagger-ui/index.html

curl -v localhost:9821/v3/api-docs
```
